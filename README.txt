WalletHub Java Test:

The deliverables are:
  -runnable jar file
  -Java source code
  -MySQL DDL scripts
  -MySQL queries


The jar file is in the 'bin' folder

The source code is a Maven based project.
In order to create the jar file the following command has to be executed at the root of the project:
$> mvn clean package

The application uses Hibernate, which is configured with a "drop-create" ddl behavior.
Each time the application runs, drops and recreates the tables.

DDLs and queries can be found in the 'test.sql' file located at the '\src\main\resources\' folder.
The 'persistence.xml' file contains the DB configuration.
A 'parser' schema is required, since Hibernate is not designed to perform such operation.


NB:
while executing the tool, if on Windows system, the 'accesslog' argument value needs to be quoted
ie: java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 --accesslog="C:\Users\joe\Desktop\javaparser\access.log"


Request for clarification:
From the requirements it is not clear if all the server access has to be logged, or only the ones performed by an IP that falls into the input criteria.
Hence two tables are created:
  Access_Log:
    contains all the server requests in the file
  Ip:
    lists all the ip satisfying the criteria