create table Log (
  id integer not null,
  request_time DATETIME,
  ip varchar(255),
  request varchar(255),
  http_response integer,
  user_agent varchar(255),
  primary key (id)
);

create table Ip (
  id integer not null,
  ip varchar(255),
  comment varchar(255),
  primary key (id)
);

--MySQL query to find IPs that mode more than a certain number of requests for a given time period
select ip, count(*)
from access_log
where request_time >= '2017-01-01.00:00:00' and request_time <= '2017-01-01.23:59:59'
and ip = '192.168.102.136'
GROUP BY ip;

--MySQL query to find requests made by a given IP
select request
from access_log
where ip ='192.168.234.82';