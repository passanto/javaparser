package com.ef;


import com.ef.model.Access;
import com.ef.model.Ip;
import com.ef.persistence.PersistenceManager;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.apache.commons.cli.*;

import java.util.Arrays;
import java.util.List;

public class Parser {

  // constants
  public static final String ACCESS_LOG = "accesslog";
  public static final String START_DATE = "startDate";
  public static final String DURATION = "duration";
  public static final String DURATION_HOURLY = "hourly";
  public static final String DURATION_DAILY = "daily";
  public static final String THRESHOLD = "threshold";
  public static final String ARGUMENT_DATA_FORMAT = "yyyy-MM-dd.HH:mm:ss";
  public static final String LOG_DATA_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
  public static final String SEPARATOR = "\\|";
  public static final SimpleDateFormat argumentSdf = new SimpleDateFormat(ARGUMENT_DATA_FORMAT);
  public static final SimpleDateFormat logSdf = new SimpleDateFormat(LOG_DATA_FORMAT);

  static List<String> durationArgsList = Arrays.asList(DURATION_HOURLY, DURATION_DAILY);

  static String accessLogFilePath;
  static String startDateInput;
  static Date startDate;
  static Date endDate;
  static String duration;
  static String thresholdInput;
  static Integer threshold;

  static HashMap<String, List<Access>> logsMap = new HashMap<>();

  public static void main(String[] args)
      throws ParseException, IOException, java.text.ParseException {
    checkArgs(args);
    parseLogFile();
  }

  private static void parseLogFile() throws IOException {
    EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
    em.getTransaction().begin();

    try (Stream<String> stream = Files.lines(Paths.get(accessLogFilePath))) {
      stream.forEach(s -> {
        Access log = createLogObject(s);
        mapLogs(log);
      });
      persistAndPrintSatisfyingLogs(em);

      em.getTransaction().commit();
      em.close();
      PersistenceManager.INSTANCE.close();
      System.out.println("Completed inserting into database.");
    }
  }

  // --------------
  // helper methods
  // --------------


  private static void persistAndPrintSatisfyingLogs(EntityManager em) {
    System.out.println("IPs with more than " + threshold + " " + duration);
    logsMap.forEach((ip, logs) -> {
      if (logs.size() > threshold) {
        Ip log = new Ip(ip, "number of requests:" + logs.size());
        System.out.println(ip + " performed " + logs.size() + " requests");
        em.persist(log);
        System.out.println(ip);
        logs.stream().forEach(l -> em.persist(l));
      }
    });
  }

  private static Access createLogObject(String s) {
    String[] res = s.split(SEPARATOR);
    Access log = new Access();
    try {
      log.setLogTime(logSdf.parse(res[0]));
    } catch (java.text.ParseException e) {
      e.printStackTrace();
    }
    log.setIp(res[1]);
    log.setRequest(res[2]);
    log.setHttpResponse(Integer.parseInt(res[3]));
    log.setUserAgent(res[4]);
    return log;
  }

  private static void mapLogs(Access log) {
    Date logDate = log.getLogTime();
    if (startDate.compareTo(logDate) * endDate.compareTo(logDate) >= 0) {
      List l = logsMap.get(log.getIp());
      if (null == l) {
        l = new ArrayList();
      }
      l.add(log);
      logsMap.put(log.getIp(), l);
    }
  }


  private static void checkArgs(String[] args) throws ParseException, java.text.ParseException {
    //Create GNU like options
    Options gnuOptions = new Options();
    gnuOptions//
        .addOption(Option.builder(ACCESS_LOG).longOpt(ACCESS_LOG).hasArg().required().build())
        .addOption(Option.builder(START_DATE).longOpt(START_DATE).hasArg().required().build())
        .addOption(Option.builder(DURATION).longOpt(DURATION).hasArg().required().build())
        .addOption(
            Option.builder(THRESHOLD).longOpt(THRESHOLD).hasArg().required().type(Integer.class)
                .build());

    CommandLineParser gnuParser = new DefaultParser();
    CommandLine cmd = gnuParser.parse(gnuOptions, args);

    accessLogFilePath = cmd.getOptionValue(ACCESS_LOG);
    checkAccessLogFile(accessLogFilePath);

    thresholdInput = cmd.getOptionValue(THRESHOLD);
    checkThresholdArgument(thresholdInput);

    startDateInput = cmd.getOptionValue(START_DATE);
    checkStartDateArgument(startDateInput);

    duration = cmd.getOptionValue(DURATION);
    checkDurationArgument(duration);
  }

  private static void checkAccessLogFile(String s) {
    File f = new File(s);
    if (!f.exists() || f.isDirectory()) {
      throw new IllegalArgumentException("Illegal value for access log file path");
    }
  }

  private static void checkStartDateArgument(String s) {
    try {
      startDate = argumentSdf.parse(s);
    } catch (java.text.ParseException e) {
      throw new IllegalArgumentException(
          START_DATE + " vale has to be as: " + ARGUMENT_DATA_FORMAT);
    }
    LocalDateTime date = new java.sql.Timestamp(startDate.getTime()).toLocalDateTime();
    if (thresholdInput.equals(DURATION_HOURLY)) {
      date.plusHours(1);
    } else {
      date.plusDays(1);
    }
    endDate = java.util.Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
  }

  private static void checkDurationArgument(String s) {
    if (!durationArgsList.contains(s)) {
      throw new IllegalArgumentException(
          "Duration has to be one of the following: " + durationArgsList);
    }
  }

  private static void checkThresholdArgument(String s) {
    try {
      threshold = Integer.parseInt(s);
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Threshold has to be an integer number");
    }
  }
}
