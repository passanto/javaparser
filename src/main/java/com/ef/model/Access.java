package com.ef.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data // lombok getters and setters
@Table(name = "access_log")
public class Access {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator="native")
  @GenericGenerator(name = "native", strategy = "native")
  private Integer id;

  @Column(name = "request_time", columnDefinition = "DATETIME")
  @Temporal(TemporalType.TIMESTAMP)
  private Date logTime;

  private String ip;

  private String request;

  @Column(name = "http_response")
  private Integer httpResponse;

  @Column(name = "user_agent")
  private String userAgent;

}
