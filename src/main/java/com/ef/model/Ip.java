package com.ef.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Data // lombok getters and setters
@Table(name = "ip")
public class Ip {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
  @GenericGenerator(name = "native", strategy = "native")
  private Integer id;

  private String ip;

  @Column(name = "comment")
  private String comment;

  public Ip(String ip, String comment) {
    this.ip = ip;
    this.comment = comment;
  }
}
